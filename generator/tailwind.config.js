module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html', '../web/**/*.php'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: '#1d4b7e',
        'primary-dark': '#133357',
        secondary: '#dee5ec',
      },
    },
    fontFamily: {
      display: ["Raleway", "sans-serif"],
      body: ["Raleway", "sans-serif"]
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
