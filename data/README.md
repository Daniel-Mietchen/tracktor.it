# Tracker data for träcktor.de

## Testing records

You can run the script locally. To do so, first install [Node.js](https://nodejs.org/en/download/) and npm. In the repository folder, run `npm i` to grab all necessary dependencies.  
To use it manually, simply run `npm test`.

## Data formats

The tracker data is located in the tracker folder. Every tracker in our database is represented by a single JSON file (named after the slug in the JSON). The JSON has to follow the schema specified in the schema-tracker.json file.

The `supervisory-authorities` folder contains data on supervisory data protection authorities. They are structured similarly to the tracker records, following the schema in the `schema-supervisory-authorities.json` file.

## Required elements

If we know from previous requests (or from a privacy policy) which identification information is needed for requests to a company, we record that under the `required-elements` key.

If `required_elements` are specified, there *should* be one element with type `name`. This does not have to be a real name but could also be a username or even an email address, if no other information is required.  
In addition, the `required_elements` should include some way for the company to respond to the request, be it an address, an email address, a phone number or something similar. 