FAQ
===

* ### An wen wendet sich *tracktor.it!*?
    
    Die Website *tracktor.it!* ist ein Beschwerdegenerator für Internetnutzer, die ihre Privatsphäre im Internet unkompliziert einfordern wollen. 
        
* ### Warum muss ich die Analyse selbst durchführen, das könnte doch das Tool?
    
    Der Verstoß muss durch den Betroffenen oder die Betroffene belegt werden, damit eine Datenschutzbeschwerde möglich ist. Eine automatische Analyse durch meinen Server wäre daher nicht ausreichend . Es gibt aber die tolle Seite [Webkoll](https://webbkoll.dataskydd.net/de), die Seiten analysiert und die Tracker anzeigt. Das ist gut geeignet für die Voranalyse. Was für die Zukunft aber sicher denkbar wäre, ist eine automatisierte Analyse einer HAR-Datei, die der Nutzer von seinem Websitebesuch gespeichert hat. Das ist technisch aber schwieriger umzusetzen.
    
* ### Man kann nur so wenige Tracker auswählen! Wie kann man das verbessern?
    
    Sobald wir dazukommen, fügen wir mehr Tracker hinzu. Wenn Du uns dabei unterstützen willst, haben wir eine [Anleitung dazu im Wiki](https://codeberg.org/rufposten/tracktor.it/wiki). 
     
* ### Wann ist ein Tracker ohne Einwilligung verboten?
    
    In den letzten zwei Jahren haben sich recht klare Angaben der Gerichte und Behörden herauskristallisiert. Von besonderer Bedeutung ist das [BGH-Urteil zu Planet49](https://juris.bundesgerichtshof.de/cgi-bin/rechtsprechung/document.py?Gericht=bgh&nr=107623). Seitdem ist klar: Die Erstellung von Nutzerprofilen für Zwecke der Werbung oder Marktforschung braucht eine Einwilligung.
    
* ### Gibt es rechtlich einen Unterschied zwischen Tracking mit Cookies und in Apps?
    Nein, denn auch wenn das [BGH-Urteil zu Planet49](https://juris.bundesgerichtshof.de/cgi-bin/rechtsprechung/document.py?Gericht=bgh&nr=107623) nur von Cookies spricht, sind die Gesetzestexte, auf die es sich bezieht, technologieneutral. In Art. 5 Abs. 3 der Richtlinie 2002/58 heißt es wörtlich: 
    
<i>"Die Mitgliedstaaten stellen sicher, dass die Speicherung von Informationen oder der Zugriff auf Informationen, die bereits im Endgerät eines Teilnehmers oder Nutzers gespeichert sind, nur gestattet ist, wenn der betreffende Teilnehmer oder Nutzer auf der Grundlage von klaren und umfassenden Informationen, die er gemäß der Richtlinie [95/46] u. a. über die Zwecke der Verarbeitung erhält, seine Einwilligung gegeben hat. Dies steht einer technischen Speicherung oder dem Zugang nicht entgegen, wenn der alleinige Zweck die Durchführung der Übertragung einer Nachricht über ein elektronisches Kommunikationsnetz ist oder wenn dies unbedingt erforderlich ist, damit der Anbieter eines Dienstes der Informationsgesellschaft, der vom Teilnehmer oder Nutzer ausdrücklich gewünscht wurde, diesen Dienst zur Verfügung stellen kann."</i>

* ### Der Seitenbetreiber bezieht sich einfach auf ein "berechtigtes Interesse". Was nun?
Ein häufiges Missverständis: Die DSGVO räumt tatsächlich ein berechtigtes Interesse bei der Datenverarbeitung ein - und was damit gemeint ist, ist nicht so einfach zu bestimmen. Aber das ist im Fall von Tracking ohne jede Bedeutung, weil dazu immer auf Cookies, IDs oder Geräteinformationen zugegriffen wird und so die strengere ePrivacy-Richtlinie gilt. Darauf beziehen sich auch unsere Anschreiben (siehe auch vorheriger Punkt). 

* ### Mein Fall ist komplizierter, wer kann mir helfen?
    
    Die Behörden prüfen immer die Rechtmäßigkeit und sollten auch beraten, wenn man in einer Sache unsicher ist. Schreib deine Überlegungen also einfach bei der Beschwerde dazu. Gerne kann man sein Anliegen auch im [DSGVO-Forum von Mike Kuketz](https://forum.kuketz-blog.de/viewforum.php?f=51) vorbringen, wo IT-Experten, Datenschutzbeauftragte und evtl. auch ich gerne Antworten geben.
    
* ### Mein Browser zeigt den Tracker in der Netzwerkanalyse nicht an?
    
	Im Idealfall ist dann natürlich auch keiner vorhanden. Es gibt aber einige Gründe, warum er nicht zu sehen ist, obwohl er eingebunden ist:
    
1. Du bist im Privatmodus deines Browsers
2. Du hast einen Adblocker-Plugin aktiviert
3. Du hast die standardmäßige Blockade von "[Elementen zur Aktivitätsverfolgung](https://support.mozilla.org/de/kb/verbesserter-schutz-blockiert-tracker-und-skripte)" (Firefox) aktiv bzw. "[websiteübergreifendes Tracking verhindert](https://support.apple.com/de-de/guide/safari/sfri40732/mac)" (Safari).
4. Du hast ein Opt-Out-Cookie
    
      
    Also lösche vor der Analyse deine Cookies und deaktiviere alle Maßnahmen zum Trackingschutz.
    
* ### Ich möchte meine Browser-Einstellungen nicht ändern
    
    Damit du nicht bestehende Einstellungen in deinem Browser ändern musst, kannst du ein neues Profil für die Trackinganalyse anlegen. In Firefox: "about:profiles" in die URL eingeben, ein neues Profil anlegen (nenne es z.B. "Trackinganalysen") und es dann starten ("Profil zusätzlich ausführen"). Folge dann den Browser-Einstellungen, die wir in der [Hilfe für Website-Prüfungen](help.php?page=webpages) empfehlen 
    
* ### Meine Eingaben erscheinen nicht in der Statistik
	Damit die Seiten in der Statistik erscheine, musst du ganz unten den Haken setzen:

	[X] Webseite in die Liste aufnehmen

	Das Feld erscheint nur, wenn du angegeben hast, dass du keine oder nur die minimale Einwilligung für Cookies gegeben hast. 

	Darüber hinaus kann es weitere Gründe geben: Lösche am besten zunächst den LocalStorage in deinem Browser für "tracktor.it", dort werden nämlich Seiten zwischengespeichert, falls du nur Kleinigkeiten änderst. Wenn das auch nichts hilft, hat vermutlich wer anderes die Seite bereits eingetragen (evtl. auch ohne Veröffentlichung). Dann erscheint sie nicht nochmal. 
    
* ### Ist ein Trackingblocker nicht schneller und effektiver?
    
    Ein Trackingblocker nutzt nur einigen wenigen Nerds, die das Wissen haben, wie man sich im Internet effektiv vor Profilbildung schützt. Mit einer Datenschutzbeschwerde kann man hingegen vielen Leuten helfen. Hinzu kommt, dass Tracking immer schwieriger technisch zu kontrollieren wird, unter anderem durch Ausweichen auf Login-Daten und IP-Adressen. 
    
* ### Erst den Trackingblocker deaktivieren und sich dann beschweren - ist das fair?  

 Zunächst ist es ganz einfach so, dass wir ja ein Recht darauf haben, keinen Trackingblocker zu nutzen. Aber das Vorgehen ist auch fair: Ein Trackingblocker kann nicht alles blockieren. Pseudonyme Profile können mit Fingerprinting, Zugriff auf Formularfelder oder über die IP-Adresse generiert werden. Deshalb ist es wichtig, dass man sich auf rechtlich einwandfreie Seiten verlassen kann, so dass das Tracking wirklich transparent und freiwillig ist, wie der Gesetzgeber es vorsieht. Damit das für sich und auch für andere sichergestellt ist, braucht man zum Prüfen einen neutralen Browser, der nicht in die Funktion der Seite eingreift. 

    
* ### Warum soll man kleine Websitebetreiber verpetzen und Google und Facebook kommen davon?
    
    Die Entscheidung, wer mit diesem Tool an eine Behörde gemeldet wird, liegt bei den Nutzern. Es gibt auch ziemlich große Unternehmen, die mit deinen Daten unrechtmäßig Geld machen. Mit dem Aufforderungsgenerator ist immerhin vorgesehen, dass niemand ohne Vorwarnung ein Bußgeld bekommt. Das Tool soll dabei vor allem den Bequemlichkeitsvorteil ausgleichen, mit dem Trackinganbieter die Datensammlung auf so vielen Seiten verbreiten konnten. Hier können die Nutzer nun ihrerseits mit ein paar Klicks die Tracker wieder entfernen lassen. So soll _tracktor.it!_ den Zustand ändern, dass beim Tracking massive Wettbewerbsvorteile durch die Missachtung von Gesetzen entstehen, während die realistische Chance auf ein Bußgeld verschwindend gering ist. Dass die irische Datenschutzbehörde die Datenschutzprobleme bei den großen Silicon-Valley-Unternehmen nicht direkt verhindert, ist natürlich auch nicht richtig. Aber deshalb muss man Datenschutzverstöße nicht überall durchgehen lassen. Hinzu kommt, dass manche Tracker auch rechtskonform sind, aber z.B. ohne Einwilligung eingebunden werden, obwohl dies vom Hersteller vorgegeben ist.
    
* ### Kann man mit Träcktor irgendwann auch eine Datenauskunft stellen?
    
    Nein, denn dafür gibt es bereits ein anderes Projekt: [datenanfragen.de](https://datenanfragen.de/)
    
* ### Wird die Beschwerde bei den Behörden überhaupt bearbeitet?
    
    Jede Beschwerde muss bearbeitet werden, die Frage ist nur wie lange das dauert. Da habe ich unterschiedliche Erfahrungen gemacht - ein paar Monate muss man in jedem Fall warten. Aber tatenlos sind die deutschen Behörden nicht. Ich hoffe natürlich, dass auch die Behörden ihre Abläufe automatisieren, damit Bußgelder in solchen Standardfällen schnell und effizient ausgesprochen sind.
    
* ### Warum machst du das/was hast du davon?
    
    Der Rahmen ist meine [journalistische Arbeit](https://rufposten.de): Ich möchte über Tracking und Datenschutz aufklären und solche Serviceleistungen für die Leserinnen und Leser bereitstellen. Ob Websitebetreiber, Trackingfirmen oder die Behörden das gut oder schlecht finden, ist dabei weniger im Fokus, aber ich bin immer offen für [Anregungen](https://rufposten.de/blog/kontakt/). Finanziell zahlt sich das kaum aus, weil meine Spendeneinnahmen noch sehr gering sind. Deshalb unterstütze meine Arbeit mit einer kleinen [Spende](https://rufposten.de/blog/spenden/) ❤️ und sichere diesen unabhängigen Dienst für ein datenschutzfreundliches Internet .