<?php

$t = json_decode(file_get_contents(__DIR__ . "/../data/localization.json"), true);

$fallback_lang = "de";
$acceptLang = ['de']; 

if (strpos( $_SERVER["SERVER_SOFTWARE"], "Development Server" ) !== false) {
    $acceptLang = ['de', 'en']; 
}

if (isset($_COOKIE['lang'])) {
    $lang = substr($_COOKIE['lang'], 0, 2);
} else {
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
}

$lang = in_array($lang, $acceptLang) ? $lang : $fallback_lang;

$langHtml = $lang; // TODO: "de-DE" !== "de ?

function getItem ($id, $lang_code) {
    global $t;

    $path = preg_split('/\./', $id);
    
    if (!$path) return null;
    
    array_unshift($path, $lang_code);
    
    $res = $t;
    for ($i=0; $i < count($path); $i++) { 
        if ($res[$path[$i]] === null) {
            return null;
        }
        $res = $res[$path[$i]];
    }

    return $res;
}

// Translation with variables
function tv (string $id, array $vars) {

    $res = t($id);

    foreach($vars as $key => $value) {
        $res = str_replace("{{{$key}}}", $value, $res);
    }


    return $res;

}

function t($id, $d = "") {
    global $lang, $fallback_lang;

    $res = getItem($id, $lang);

    if ($res === null) {
        $res  = getItem($id, $fallback_lang);
    }

    if ($res === null) {
        return $d;
    }

    return $res;
}