<?php

function get_tracker_data () {

    $trackersWeb = json_decode(file_get_contents(__DIR__ . "/../data/tracker-webpage.json"), true);
    $trackersApp = json_decode(file_get_contents(__DIR__ . "/../data/tracker-app.json"), true);

    $trackers = [];
    foreach ($trackersWeb as $key => $tracker) {
        $tracker["type"] = "Webseite";
        array_push($trackers, $tracker);
    }
    foreach ($trackersApp as $key => $tracker) {
        $tracker["type"] = "Android App";
        array_push($trackers, $tracker);
    }

    usort($trackers, function ($a, $b) {
        return strcmp($a["name"], $b["name"]);
    });

    return $trackers;

}