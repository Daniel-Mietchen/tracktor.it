<?php if (!$included) die(); ?>

<!DOCTYPE html>
<html lang="<?php echo $langHtml; ?>">
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<?php if(isset($html_title)): ?>
		<title><?php echo $html_title; ?> - tracktor.it!</title>
<?php else: ?>
		<title><?php echo t("ge­ne­rell.html-title"); ?> - tracktor.it!</title>
<?php endif; ?>

	<meta name="description" content="Tracktor.it! ist ein DSGVO-Beschwerdegenerator für ein Internet ohne Tracking." /> 
	<meta name="robots" content="index, follow" /> 
	<meta name="referrer" content="no-referrer"> 
	
	<meta name="twitter:card" content="summary">
	<meta property="twitter:title" content="&#x1f69c;-&#x2709;&#xFE0F;&nbsp;&nbsp;tracktor.it!&nbsp;&nbsp;&nbsp;&#x1f69c;-&#x2709;&#xFE0F;">
	<meta name="twitter:image" content="https://tracktor.it/assets/images/ricky_the_greedy_racoon_1024px.png">
	<meta property="twitter:description" content="<?php echo t("ge­ne­rell.twitter.desc"); ?>">
	
	<meta property="og:title" content="&#x1f69c;-&#x2709;&#xFE0F;&nbsp;&nbsp;tracktor.it!&nbsp;&nbsp;&nbsp;&#x1f69c;-&#x2709;&#xFE0F;">
	<meta property="og:url" content="https://tracktor.it/">
	<meta property="og:image" content="https://tracktor.it/assets/images/ricky_the_greedy_racoon_1024px.png">
	<meta property="og:description" content="<?php echo t("ge­ne­rell.twitter.desc"); ?>">



	<link rel="apple-touch-icon" sizes="180x180" href="assets/icons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/icons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/icons/favicon-16x16.png">
	<link rel="manifest" href="./site.webmanifest">
	<link rel="canonical" href="https://www.tracktor.it/" /> 

</head>
