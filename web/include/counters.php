<?php
if(!$included) die();


require_once __DIR__ . "/../core/utils.php";
require_once __DIR__ . "/../statistics/services.php";
?>

<section class="max-w-screen-lg mx-auto grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4 px-4 mb-16">

<?php

if ($front_page) {

    $trackers = get_tracker_data();
    $stats = get_count_of_generation();

    $counters = [
        [
            "label" => t("home.generated-requests"),
            "count" => $stats->generated_requests
        ],
        [
            "label" => t("home.generated-complaints"),
            "count" => $stats->generated_complaints
        ],
        [
            "label" => t("home.tracker-in-db"),
            "count" => count($trackers)
        ]
    ];

} else {

    $stats = get_count_of_unique_entries();

    $counters = [
        [
            "label" => t("statistics.total-generated"),
            "count" => $stats->entries_total
        ],
        [
            "label" => t("statistics.various-websites"),
            "count" => $stats->unique_domains
        ],
        [
            "label" => t("statistics.various-apps"),
            "count" => $stats->unique_apps
        ]
    ];

}

?>

<?php foreach ($counters as $key => $value): ?>
		<a href="/statistics.php">
			<div class="rounded shadow-lg p-6 mb-4" style="min-width: 181px; ">
				<p class="text-primary text-4xl md:text-6xl font-medium count-up"><?php echo $value["count"]; ?></p>
				<p class="pt-2"><?php echo $value["label"]; ?></p>
			</div>
		</a>
<?php endforeach; ?>

</section>

<script defer src="assets/js/count.js"> </script>

<script>

    let countMeUp = [...document.querySelectorAll(".count-up")];
    countMeUp.forEach(el => {
        el.dataset.to = parseInt(el.innerHTML);
        el.innerHTML = "0";
    });

</script>
