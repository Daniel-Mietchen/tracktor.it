<?php if (!$included) die(); ?>

<?php // to avoid shadow-md being removed ?>
<div class="shadow-md hidden"></div>

<header class="fixed min-w-full z-20 bg-white">
    <div class="p-4 max-w-screen-lg mx-auto flex items-center justify-between">

        <div class="flex items-center">
            <a href="/" class="flex items-center space-x-2">
                <span class="sr-only"><?php echo t("navigation.home"); ?></span>
                <img class="w-auto h-9" src="assets/images/traktor_email_logo_300px.png" alt="Traktor-Email Logo">
                <img class="w-auto h-9 hidden sm:block" src="assets/images/tracktor_logo_type.png" alt="tracktor.it!">
                <img class="w-auto h-7 block sm:hidden" src="assets/images/tracktor_logo_type.svg" alt="tracktor.it!">
            </a>
        </div>

        <button class="block md:hidden" onclick="toggleMenu(this)">
          <i class="icon menu"></i>
          <i class="icon close hidden"></i>
        </button>

        <nav id="menu" class="fixed bg-white inset-0 top-14 md:inset-auto p-4 md:p-0 md:relative hidden md:flex flex-col md:flex-row md:space-x-4 items-center text-lg">
            <a class="block" href="/help.php"><?php echo t("navigation.faq"); ?></a>
            <a class="block" href="/tracker.php"><?php echo t("navigation.tracker-database"); ?></a>
            <a class="block" href="https://rufposten.de/blog/spenden/"><span><?php echo t("navigation.donate"); ?></span>&nbsp;<i class="icon heart align-middle"></i></a>

            <?php if (strpos( $_SERVER["SERVER_SOFTWARE"], "Development Server" ) !== false): ?>

            <div class="relative w-24">
              <select class="border-none appearance-none cursor-pointer pr-5 -ml-1" onchange="changeLang(this)">
                  <option value="de" <?php echo ($lang === "de") ? "checked" : ""; ?>>Deutsch</option>
                  <option value="en" <?php echo ($lang === "en") ? "checked" : ""; ?>>English</option>
              </select>
              <i class="icon arrow-down absolute top-1 right-0 pointer-events-none"></i>
            </div>

            <?php endif; ?>
        </nav>

    </div>
</header>

<div class="h-36"></div>
