<?php 

require_once "./core/localization.php";

$included = true;

$page = "webpages";
$language_used = $lang;
$index = (array) @json_decode(@file_get_contents(__DIR__ . "/data/help-pages/$lang/index.json"));

if (count($index) === 0) {
    $language_used = $fallback_lang;
    $index = (array) @json_decode(@file_get_contents(__DIR__ . "/data/help-pages/$fallback_lang/index.json"));
}


if (isset($_GET["page"]) && preg_match("/([a-z0-9-]{0,30})/", (string) $_GET["page"], $matches) == 1) {

    if ($index[$matches[0]]) {
        $page = $matches[0];
        
    }

}


$html_title = "Hilfe";

if ($page !== "") {
    $page_data = $index[$page];
    $html_title = $page_data->title;
}

require "./include/header.php";

?>

<body>

<link rel="stylesheet" href="./assets/css/highlight-github.css">
<script defer src="./assets/js/highlight.pack.js"></script>

<?php require_once "./include/navbar.php"; ?>


    
    <div class="max-w-screen-lg mx-auto px-6 lg:px-0">

        <hr>

        <!-- <h1 class="text-primary text-3xl md:text-4xl font-medium" style="margin: 55px 20px 20px">Hilfe-Seiten</h1> -->

        <div class="mx-auto grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4 px-4 my-16">


<?php foreach  ($index as $key => $value): ?>

        <a href="/help.php?page=<?php echo $key; ?>">

            <div class="border-2 p-8 mb-4 <?php echo ($key === $page) ? "bg-gray-100 border-gray-200" : "border-gray-100 "; ?>">

                <p class="text-center text-xl"> <?php echo $value->title; ?></p>

            </div>
        
        </a>

<?php endforeach; ?>

        </div>

        <hr >

        <div class="h-10"></div>

        <?php echo @file_get_contents(__DIR__ . "/data/help-pages/$language_used/$page_data->file.html"); ?>

        <div class="h-10"></div>
        <div class="border-2 border-gray-100 p-5 my-8 relative">

            <p class="text-center text-xl font-medium line-break">
                <?php echo t("help.improve-page");?>
            </p>

            <p class="text-center m-10">
                <a class="btn-secondary" href="https://codeberg.org/rufposten/tracktor.it/src/branch/master/data/help-pages/<?php echo $language_used; ?>/<?php echo $page_data->file; ?>.md">
                    <?php echo t("help.edit-on-codeberg");?>
                </a>
            </p>

        </div>

    </div>


<?php require_once "./include/footer.php"; ?>


<script>

window.onload = () => {
    hljs.highlightAll();
}

</script>

</body>
</html>