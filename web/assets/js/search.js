function toggleDetails (slug) {
    [...document.querySelectorAll('[data-searchme=\''+slug+'\']')].forEach(e => e.classList.toggle('hidden'))
}

function filterList(input) {
    const items = [...document.querySelectorAll("ul.list li")];
    const notfound = items.find(e => e.className.indexOf("notfound") > -1);
    let found = 0;
    items.forEach(item => {
        if (input.value === "" || item.querySelector("h3").innerText.toLowerCase().indexOf(input.value.toLowerCase()) > -1) {
            item.classList.remove("hidden");
            found++;
        } else {
            item.classList.add("hidden");
        }
    });
    notfound.classList[(found > 0) ? "add" : "remove"]("hidden");
}