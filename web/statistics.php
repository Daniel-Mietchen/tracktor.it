<?php

require_once "./core/localization.php";

$included = true;
$html_title = t("statistics.title");

require "./include/header.php";
require_once "./statistics/services.php";

$page = 0;
$query = "";
$max_pagination = 10;

if (isset($_GET["query"])) {
    $query = htmlentities((string) $_GET["query"]);
}

$captured_websites_and_apps = get_captured_websites_and_apps($query);
$hit_count = count($captured_websites_and_apps);
$pages_total = ceil($hit_count / $max_pagination);


if (isset($_GET["page"])) {
    $page_tmp = (int) $_GET["page"];
    if (0 < $page_tmp && $page_tmp <= $pages_total) {
        $page = $page_tmp - 1;
    }
}

$found_items = array_slice($captured_websites_and_apps, ($page*$max_pagination), $max_pagination);

?>

<body>

    <?php require_once "./include/navbar.php" ?>

    <div class="max-w-screen-lg mx-auto px-6 lg:px-0">

        <h1 class="text-primary text-3xl md:text-4xl font-medium"><?php echo t("statistics.title"); ?></h1>
        <p class="my-3 text-lg"><?php echo t("statistics.desc"); ?></p>

        <div class="h-10"></div>

<?php 
// Do not show again once the page / searched for
if (!isset($_GET["query"])) {
    require "./include/counters.php";
}
?>

        <form methode="GET">
            <input name="query" type="text" value="<?php echo $query; ?>" class="my-3 rounded border-2 px-4 py-2 w-full sm:w-1/2 focus:border-primary" placeholder="<?php echo t("statistics.search-placeholder"); ?>"/>
            <input hidden name="page" value="<?php echo $page+1; ?>" />
            <button type="submit" class="btn-primary" style="padding: 7px 15px;" ><?php echo t("statistics.button-search"); ?></button>
        </form>

        <p class="font-mono text-xs">

<?php if ($query === "") {
            echo tv("statistics.search-no-query", [ "count" =>  $hit_count ]);
} else if ($hit_count > 1) {
            echo tv("statistics.search-several-results", [ "count" => $hit_count, "query" => $query ]);
} else if ($hit_count === 1) {
            echo tv("statistics.search-one-result", [ "query" => $query ]); 
} ?>

        </p>

        <ul class="list" style="min-height: 50vh">

<?php if ($hit_count === 0 && $query !== ""): ?>

    <li class="border-2 border-gray-100 p-4 my-5 relative">
        <h3 class="text-xl font-medium">
            <?php echo t("statistics.search-no-result"); ?>
        </h3>

        <p class="my-2">
            <a class="link text-right" href="/#generator" rel="noopener noreferrer">
                <?php echo t("statistics.link-generate-now"); ?>
            </a>
        </p>
    </lI>

<?php endif; ?>

<?php foreach ($found_items as &$item): ?>

            <li class="border-2 border-gray-100 p-4 my-5 relative">

                <h3 class="text-xl font-medium">
                    <?php echo (isset($item["domain"])) ? $item["domain"] :  $item['app_id'] . " (Version: " . $item['app_version'] . ")"; ?>
                </h3>

                <div class="absolute top-7 right-7 px-4 py-2 bg-secondary rounded">
                    <?php echo (isset($item["domain"])) ? "Webseite" :  "Android App"; ?>
                </div>
                <p class="my-1">
                    Am <?php echo get_date_from_db_time($item["created_at"]); ?> wurde eine <b><?php echo ($item["action"] === "complaint") ? "Beschwerde" : "Aufforderung"; ?></b> generiert.
                </p>

            </li>

<?php endforeach; ?>

        </ul>

<?php

$max = $pages_total;
$i = $page;
$max = $i + 3;
$i = $i - 1;

while($i < 1) {
    $i++;
    $max++;
}
while ($max > $pages_total) {
    $i--;
    $max--;
}

function get_pagination_icon ($i, $text = "") {
    global $page, $query;

    $bg = ($i === $page+1) ? "bg-gray-100" : "bg-white";

    if ($i === -1) {

        ?>
        <div class="relative inline-flex items-center px-4 py-2 border border-gray-300  text-sm font-medium text-gray-700">
            <?php echo $text; ?>
        </div>
        <?php

    } else {
        ?>
        <a href="/statistics.php?query=<?php echo $query; ?>&page=<?php echo $i; ?>" class="<?php echo $bg; ?> relative inline-flex items-center px-4 py-2 border border-gray-300  text-sm font-medium text-gray-700 hover:bg-gray-50">
            <?php echo $i; ?>
        </a>
        <?php
    }

}

?>


<?php if ($pages_total > 1): ?>

        <nav class="relative z-0 inline-flex rounded-md shadow-sm -space-x-px" aria-label="Pagination">

<?php if($page > 0): ?>
        
            <a href="/statistics.php?query=<?php echo $query; ?>&page=<?php echo $page; ?>" class="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                <span class="sr-only">Previous</span>
                <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
                </svg>
            </a>
<?php endif; ?>

<?php
if($i > 2) {
    get_pagination_icon(1);
    get_pagination_icon(-1, "...");
} 
for (;$i <= $max; $i++) {
    get_pagination_icon($i);
}
if($max < $pages_total) {
    get_pagination_icon(-1, "...");
    get_pagination_icon($pages_total);
} 
?>

<?php if(($page+1) < $pages_total): ?>
        
            <a href="/statistics.php?query=<?php echo $query; ?>&page=<?php echo $page+2; ?>" class="relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50">
                <span class="sr-only">Next</span>
                <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                </svg>
            </a>

<?php endif; ?>

      </nav>

<?php endif; ?>

    </div>

    <?php require_once "./include/footer.php" ?>

</body>
</html>
