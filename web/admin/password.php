<?php
if (isset($_POST["password"])) {
    if (strlen($_POST["password"]) < 12) {
        echo "Passwort muss mehr als 12 Zeichen haben!";
    } else {
        echo "Passwort: " . password_hash((string) $_POST["password"], PASSWORD_DEFAULT);
        die();
    }
}
?>

<form action="password.php" method="POST">
    <input type="text" name="password" placeholder="Passwort eingeben" />
    <button type="submit">Passwort erstellen</button>
</form>