<?php

require_once __DIR__ . "/config.php";

if (!isset($users)) {
    die("config.php not exist!");
}

function unauthorized () {
    header('WWW-Authenticate: Basic realm="Admin panel"');
    header('HTTP/1.0 401 Unauthorized');
    echo '401 Unauthorized';
    die();
}

if (!isset($_SERVER['PHP_AUTH_USER'])) {
    unauthorized();
}

$user = (string) $_SERVER['PHP_AUTH_USER'];
$password = (string) $_SERVER['PHP_AUTH_PW'];

$isdev = strpos( $_SERVER["SERVER_SOFTWARE"], "Development Server" ) !== false;

if( $password === "admin" && !$isdev) {
    echo "In der Produktivumgebung ist das Standardpasswort nicht erlaubt!";
    exit;
}
if (!$isdev && strlen($password) < 12) {
    echo "In der Produktivumgebung muss das Passwort mehr als 12 Zeichen haben!";
    exit;
}

if (!isset($users[$user]) || !password_verify ($password, $users[$user]) ) {
    unauthorized();
}

require_once "../statistics/services.php";

if (isset($_POST["action"])) {
    $action = (string) $_POST["action"];

    if ($action === "delete-entry" && isset($_POST["entry_id"])) {

        $entry_id = htmlentities((string) $_POST["entry_id"]);
        $table = (string) $_POST["table"];

        if (delete_complete_entry($table, $entry_id)) {
            echo "<h1>($entry_id) Erfolgreich gelöscht!</h1>";
        } else {
            echo "<h1>Fehler beim Löschen von $entry_id!</h1>";
        };

    }
    
}

$entries = get_all_entries_for_admin();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin-Panel</title>

    <style>
        body {
            margin: 10px auto;
            max-width: 1000px;
        }
        table { width: 100%; }
        form { margin: 0 }
    </style>

</head>
<body>

<?php foreach(["webpage", "app"] as &$key): ?>

<?php if (count($entries[$key]) > 0): ?>
<h1><?php echo $key; ?></h1>
<table border="1">
  <thead>
    <tr>
      <th><?php echo implode('</th><th>', array_keys(current($entries[$key]))); ?></th>
      <th>Löschen</th>
    </tr>
  </thead>
  <tbody>
<?php foreach ($entries[$key] as $row): array_map('htmlentities', $row); ?>
    <tr>
        <td><?php echo implode('</td><td>', $row); ?></td>
        <td>
        <form action="/admin/index.php" method="POST" onsubmit="return confirm('Achtung! Beim Löschen wird der gesamte Eintrag (alle mit der gleichen entry_id) gelöscht!')">
            <input hidden name="entry_id" value="<?php echo $row["entry_id"]; ?>" />
            <input hidden name="table" value="<?php echo $key; ?>" />
            <input hidden name="action" value="delete-entry" />
            <button type="submit">Löschen</button>
        </form>
        </td>
    </tr>
<?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>

<?php endforeach; ?>
      
</body>
</html>