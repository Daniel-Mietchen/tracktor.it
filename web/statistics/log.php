<?php

function log_error ($message) {

    $time = date('Y-m-d H:i:s');
    $message = "[$time] " . $message;
    file_put_contents(__DIR__ . "/error.txt", $message . "\n", FILE_APPEND);

}